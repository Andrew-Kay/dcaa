class ListSet:
	def __init__(self,values=None):
		if values is None:
			self.elements = []
		else:
			self.elements = list(values)
	def __contains__(self, x):
		return x in self.elements
	def __eq__(self, other):
		for x in self.elements:
			if x not in other.elements:
				return False
		for x in other.elements:
			if x not in self.elements:
				return False
		return True
	def add(self, x):
		if x not in self.elements:
			self.elements.append(x)
	def remove(self, x):
		self.elements.remove(x)
	def union(self, other):
		result = ListSet()
		for x in self.elements:
			result.add(x)
		for x in other.elements:
			result.add(x)
		return result
	def intersection(self, other):
		result = ListSet()
		for x in self.elements:
			if x in other:
				result.add(x)
		return result
	def difference(self, other):
		result = ListSet()
		for x in self.elements:
			if x not in other:
				result.add(x)
		return result
#
