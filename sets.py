from logic import exactly_one

def take_from(S):
	try:
		return next(iter(S))
	except StopIteration:
		raise ValueError("Can't take from an empty set.")
#

def product(A,B):
	return frozenset( (a,b) for a in A for b in B )
def power_set(A):
	if len(A) == 0:
		return frozenset({ frozenset() })
	x = take_from(A)
	PB = power_set(A - {x})
	return frozenset( S|{x} for S in PB ) | PB
def all_relations(A,B):
	return frozenset( Relation(R,A,B) for R in power_set(product(A,B)) )
def all_functions(A,B):
	if len(A) == 0:
		return frozenset({ Function(frozenset(),A,B) })
	a = take_from(A)
	return frozenset( Function(f|{(a,b)},A,B) for f in all_functions(A-{a},B) for b in B )
#

class Relation(frozenset):
	def __new__(cls, *args):
		if len(args) == 0 or args[0] is None:
			return super(Relation, cls).__new__(cls)
		else:
			return super(Relation, cls).__new__(cls, args[0])
	def __init__(self, vals=None, source=None, target=None):
		if source is not None:
			if not (self.domain() <= source): raise ValueError("Invalid source set.")
			self.source = frozenset(source)
		elif isinstance(vals, Relation):
			self.source = vals.source
		else:
			self.source = self.domain()
		if target is not None:
			if not (self.range() <= target): raise ValueError("Invalid target set.")
			self.target = frozenset(target)
		elif isinstance(vals, Relation):
			self.target = vals.target
		else:
			self.target = self.range()
	def domain(self):
		return frozenset( x for (x,y) in self )
	def range(self):
		return frozenset( y for (x,y) in self )
	def inverse(self):
		return Relation([(y,x) for (x,y) in self], self.target, self.source)
	def image_of(self, S):
		if S <= self.source:
			return frozenset( y for (x,y) in self if x in S )
		else:
			raise ValueError("Can't take relational image of this set.")
	def union(self,other):
		U = frozenset.union(self, other)
		if isinstance(other, Relation) and self.source == other.source and self.target == other.target:
			return Relation(U, self.source, self.target)
		else:
			return U
	def __or__(self,other):
		return self.union(other)
	def intersection(self,other):
		U = frozenset.intersection(self, other)
		if isinstance(other, Relation) and self.source == other.source and self.target == other.target:
			return Relation(U, self.source, self.target)
		else:
			return U
	def __and__(self,other):
		return self.intersection(other)
	def difference(self,other):
		U = frozenset.difference(self, other)
		if isinstance(other, Relation) and self.source == other.source and self.target == other.target:
			return Relation(U, self.source, self.target)
		else:
			return U
	def __sub__(self,other):
		return self.difference(other)
	def __mul__(self, other):
		if self.target <= other.source:
			comp = [(x,z) for (x,y1) in self for (y2,z) in other if y1 == y2]
			return Relation(comp, self.source, other.target)
		else:
			raise ValueError("Can't compose these relations.")
	def __pow__(self, n):
		S,n = (self,n) if n>=0 else (self.inverse(),-n)
		R = Function.identity(S.source)
		if n == 0: return R
		elif n == 1: return S
		# square-and-multiply algorithm
		while n > 0:
			if n&1 == 1:
				R = R*S
			S = S*S
			n >>= 1
		return R
	def is_partial_function(self):
		return all( exactly_one( (x,y) in self for y in self.range() ) for x in self.domain() )
	def is_function(self):
		return self.source == self.domain() and self.is_partial_function()
	def is_injective(self):
		return all( exactly_one( (x,y) in self for x in self.domain() ) for y in self.range() )
	def is_surjective(self):
		return self.range() == self.target
	def is_bijective(self):
		return self.is_function() and self.is_injective() and self.is_surjective()
	def require_A_to_A(self):
		if self.source != self.target: raise ValueError()
	def is_reflexive(self):
		self.require_A_to_A()
		return Function.identity(self.source) <= self
	def is_symmetric(self):
		self.require_A_to_A()
		return self == self.inverse()
	def is_transitive(self):
		self.require_A_to_A()
		return self*self <= self
	def is_equivalence_relation(self):
		return self.is_reflexive() and self.is_symmetric() and self.is_transitive()
	def reflexive_closure(self):
		self.require_A_to_A()
		return self | Function.identity(self.source)
	def symmetric_closure(self):
		self.require_A_to_A()
		return self | self.inverse()
	def transitive_closure(self):
		self.require_A_to_A()
		R = self
		RcompR = R*R
		while not (RcompR <= R):
			R = R | RcompR
			RcompR = R*R
		return R
	def equivalence_closure(self):
		return self.reflexive_closure().symmetric_closure().transitive_closure()
#

class Function(Relation):
	@staticmethod
	def identity(A):
		return Function([(x,x) for x in A], A, A)
	#
	
	def __init__(self, vals=None, source=None, target=None):
		Relation.__init__(self, vals, source, target)
		if not Relation.is_function(self):
			raise ValueError("Not a function: some input did not have exactly one output.")
	def __call__(self,x):
		if x in self.source:
			return take_from( self.image_of({x}) )
		else:
			raise ValueError("Can't evaluate function on this input.")
	def __mul__(self,other):
		R = Relation.__mul__(self,other)
		return Function(R, R.source, R.target) if isinstance(other, Function) else R
	def __pow__(self,n):
		R = Relation.__pow__(self,n)
		return Function(R, R.source, R.target)
	def is_function(self):
		return True
#
