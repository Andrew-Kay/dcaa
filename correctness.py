from copy import deepcopy
from functools import wraps

class PreconditionFailure(AssertionError): pass
class PostconditionFailure(AssertionError): pass
class InvariantFailure(AssertionError): pass
class VariantFailure(AssertionError): pass

def input_type(*vtypes, **kwtypes):
	def decorator(f):
		@wraps(f)
		def func_wrapper(*vargs, **kwargs):
			if len(vargs) > len(vtypes):
				raise TypeError('Too many positional arguments.')
			for i in range(0, len(vargs)):
				a,t = vargs[i], vtypes[i]
				if not isinstance(a, t):
					raise TypeError('Invalid argument type: was {0}, should be {1}'.format(type(a).__name__, t.__name__))
			for k,a in kwargs.items():
				if k not in kwtypes:
					raise TypeError('Unexpected keyword-argument with key {0}'.format(repr(k)))
				t = kwtypes[k]
				if not isinstance(a, t):
					raise TypeError('Invalid keyword-argument type for key {2}: was {0}, should be {1}'.format(type(a).__name__, t.__name__, repr(k)))
			return f(*vargs, **kwargs)
		return func_wrapper
	return decorator
#

def output_type(t, or_none=False):
	def decorator(f):
		@wraps(f)
		def func_wrapper(*vargs, **kwargs):
			output = f(*vargs, **kwargs)
			if not isinstance(output, t) and not (or_none and output is None):
				raise TypeError('Invalid argument type: was {0}, should be {1}'.format(type(output).__name__, t.__name__))
			return output
		return func_wrapper
	return decorator
#

def precondition(predicate, msg='A precondition was not satisfied.'):
	def decorator(f):
		@wraps(f)
		def func_wrapper(*vargs, **kwargs):
			if not predicate(*vargs, **kwargs):
				raise PreconditionFailure(msg)
			return f(*vargs, **kwargs)
		return func_wrapper
	return decorator
#

def postcondition(predicate, msg='A postcondition was not satisfied.'):
	def decorator(f):
		@wraps(f)
		def func_wrapper(*vargs, **kwargs):
			vargs_copy, kwargs_copy = deepcopy(vargs), deepcopy(kwargs)
			output = f(*vargs, **kwargs)
			if not predicate(output, *vargs_copy, **kwargs_copy):
				raise PostconditionFailure(msg)
			return output
		return func_wrapper
	return decorator
#

class Invariant:
	def __init__(self, predicate, msg='An invariant was not satisfied.'):
		self.predicate = predicate
		self.msg = msg
		self.check()
	def check(self):
		if not self.predicate():
			raise InvariantFailure(self.msg)
		return True
	def __and__(self,other):
		return Invariants(self, other)
#

class LoopVariant:
	def __init__(self, expression):
		self.expression = expression
		self.last_variant = None
		self.check()
	def check(self):
		new_variant = self.expression()
		if not isinstance(new_variant, int):
			raise VariantFailure('Loop variant must be an int.')
		elif new_variant < 0:
			raise VariantFailure('Loop variant must be non-negative.')
		elif self.last_variant is not None and new_variant >= self.last_variant:
			raise VariantFailure('Loop variant must decrease on each iteration.')
		self.last_variant = new_variant
		return True
#

def recursion_variant(expression):
	variant_stack = []
	def decorator(f):
		@wraps(f)
		def func_wrapper(*vargs, **kwargs):
			new_variant = expression(*vargs, **kwargs)
			if not isinstance(new_variant, int):
				raise VariantFailure('Recursion variant must be an int.')
			elif new_variant < 0:
				raise VariantFailure('Recursion variant must be non-negative.')
			elif len(variant_stack) > 0 and new_variant >= variant_stack[-1]:
				raise VariantFailure('Recursion variant must decrease on each recursive call.')
			variant_stack.append(new_variant)
			try:
				output = f(*vargs, **kwargs)
				variant_stack.pop()
				return output
			except:
				variant_stack.pop()
				raise
		return func_wrapper
	return decorator
#
