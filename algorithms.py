# searching algorithms

def linear_search(L,x):
	i = 0 # start at the first item
	n = len(L)
	while i < n:
		if L[i] == x: # found x
			return i
		i += 1 # move onto the next item
	return None # if we reach here, x is not in the list
#

def linear_search_min(L):
	m = L[0] # lowest so far
	i = 1
	n = len(L)
	while i < n:
		if L[i] < m: # found something lower
			m = L[i]
		i += 1 # move onto the next item
	return m
#

def linear_search_max(L):
	m = L[0] # highest so far
	i = 1
	n = len(L)
	while i < n:
		if L[i] > m: # found something higher
			m = L[i]
		i += 1 # move onto the next item
	return m
#

def binary_search(L,x):
	a,b = 0, len(L)-1
	while a <= b:
		m = (a+b) // 2
		if x == L[m]: # found x
			return m
		elif x < L[m]: # too high - look to the left next time
			b = m-1
		else: # too low - look to the right next time
			a = m+1
	return None # if we reach here, x is not in the list
#


# sorting algorithms	

def selection_sort(L):
	n = len(L)
	i = 0
	while i < n-1:
		m = i
		j = i+1
		while j < n: # linear search for the index of the lowest remaining value
			if L[j] < L[m]:
				m = j
			j += 1
		L[i], L[m] = L[m], L[i] # swap the lowest remaining value to the correct place
		i += 1
	return L
#

def bubble_sort(L): # a more efficient implementation would stop if no swaps are made in a full pass
	n = len(L)
	i = 0
	while i < n-1:
		j = 0
		while j < n-1-i:
			if L[j] > L[j+1]: # swap adjacent values as appropriate
				L[j], L[j+1] = L[j+1], L[j]
			j += 1
		i += 1
	return L
#

def insertion_sort(L):
	n = len(L)
	i = 1
	while i < n:
		j = i # start at L[i]
		while j > 0 and L[j-1] > L[j]: # swap this value to the left, until it's in the right place
			L[j-1], L[j] = L[j], L[j-1]
			j -= 1
		i += 1
	return L
#


# used for the @trace_calls decorator; try print_call_tree() after calling one of these functions
from tracer import *

# merge (sorted) lists A and B into a single (sorted) list; used in the merge sort algorithm
@trace_calls
def linear_merge(A,B):
	a,b = len(A), len(B)
	n = a + b
	L = [0]*n # we already know how long L will be, so let's allocate the memory all at once
	i,j = 0,0 # i counts our position in A, j counts our position in B
	while i + j < n:
		if i < a and (j == b or A[i] <= B[j]):
			L[i+j] = A[i]
			i += 1
		else:
			L[i+j] = B[j]
			j += 1
	return L
#

@trace_calls
def merge_sort(L):
	n = len(L)
	if n <= 1: # base case
		return list(L)
	m = n // 2 # split the list into two halves
	A = L[:m]
	B = L[m:]
	A = merge_sort(A) # sort each half
	B = merge_sort(B)
	return linear_merge(A,B) # merge the results
#

@trace_calls
def quicksort(L):
	n = len(L)
	if n <= 1: # base case
		return list(L)
	p = L[0] # choose a pivot element; L[0] is the simplest choice, but often not the best
	A,B = [],[]
	i = 1
	while i < n:
		v = L[i]
		if v <= p: # put everything <= p into A
			A.append(v)
		else: # put everything > p into B
			B.append(v)
		i += 1
	A = quicksort(A) # sort each side of the pivot
	B = quicksort(B)
	return A + [p] + B # join the results
#
