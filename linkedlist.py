class ListItem:
	def __init__(self, value=None, nxt=None):
		self.value = value
		self.nxt = nxt # next is a keyword in Python, so we'll use nxt instead.
#

class LinkedList(ListItem):
	# constructor
	def __init__(self, values=None):
		ListItem.__init__(self)
		if values is not None:
			self.extend(values)
	#
	
	# helper method
	def item_at_index(self, index):
		if index < 0: raise IndexError('list index out of range')
		a,i = self,-1 # self is the head, self.nxt is the 0th item
		while i < index:
			if a.nxt is None: raise IndexError('list index out of range')
			a = a.nxt
			i += 1
		return a
	# helper method
	def last_item(self):
		a = self
		while a.nxt is not None:
			a = a.nxt
		return a
	#
	
	# the following methods implement standard list operations.
	# most programming languages with (mutable) lists will have most of these operations.
	
	# length of the list, implements len(self)
	def __len__(self):
		a,n = self,0
		while a.nxt is not None:
			a = a.nxt
			n += 1
		return n
	# read from the list
	def __getitem__(self, index):
		if isinstance(index, int): # implements self[index]
			return self.item_at_index(index).value
		elif isinstance(index, slice): # implements self[start:stop:step]
			return self.get_slice(index.start, index.stop, index.step)
		else:
			raise TypeError('list indices must be integers or slices, not {0}'.format(type(index).__name__))
	# write to the list, implements self[index] = x
	def __setitem__(self, index, x):
		if isinstance(index, int):
			self.item_at_index(index).value = x
		else:
			raise TypeError('list indices must be integers, not {0}'.format(type(index).__name__))
	# delete one item from the list, implements del self[index]
	def __delitem__(self, index):
		if isinstance(index, int):
			a = self if index == 0 else self.item_at_index(index-1)
			if a.nxt is None:
				raise IndexError('list assignment index out of range')
			a.nxt = a.nxt.nxt
		else:
			raise TypeError('list indices must be integers, not {0}'.format(type(index).__name__))
	# insert the value x at the start of the list
	def prepend(self, x):
		self.nxt = ListItem(x, self.nxt)
	# insert the value x at the end of the list
	def append(self, x):
		self.last_item().nxt = ListItem(x)
	# insert the value x at an index
	def insert(self, index, x):
		a = self if index == 0 else self.item_at_index(index-1)
		a.nxt = ListItem(x, a.nxt)
	# determine whether the list contains the value x, implements (x in self), (x not in self)
	def __contains__(self, x):
		a = self
		while a.nxt is not None:
			a = a.nxt
			if a.value == x:
				return True
		return False
	# return the index of the first instance of the value x in the list
	def index(self, x):
		a = self
		i = 0
		while a.nxt is not None:
			a = a.nxt
			if a.value == x:
				return i
			i += 1
		raise ValueError('{0} is not in list'.format(x))
	# count how many times the value x appears in the list
	def count(self, x):
		a = self
		c = 0
		while a.nxt is not None:
			a = a.nxt
			if a.value == x:
				c += 1
		return c
	#
	
	# the following methods implement additional features of Python lists, so that
	# LinkedList objects behave in (almost) the same way as a Python lists.
	
	# remove all items from the list
	def clear(self):
		self.nxt = None
	# insert multiple values at the end of the list
	def extend(self, values):
		# avoid an infinite loop...
		if values is self: values = self.copy()
		# for x in values: self.append(x)
		# would be equivalent, but less efficient because it would call last_item() many times
		a = self.last_item()
		for x in values:
			a.nxt = ListItem(x)
			a = a.nxt
	# implements self += other
	def __iadd__(self, other):
		self.extend(other)
	# implements self + other
	def __add__(self, other):
		b = self.copy()
		b.extend(other)
		return b
	# implements other + self
	def __radd__(self, other):
		b = LinkedList(other)
		b.extend(self)
		return b
	# create a new list with the values repeated n times, implements self * n
	def __mul__(self, n):
		b_head = b = LinkedList()
		i = 0
		while i < n:
			a = self
			# more efficient than b_head.extend(a), since we don't call last_item() n times
			while a.nxt is not None:
				a = a.nxt
				b.nxt = ListItem(a.value)
				b = b.nxt
			i += 1
		return b_head
	# implements self == other
	# Python lists have structural equality, rather than refential equality
	def __eq__(self, other):
		if not isinstance(other, (list, LinkedList)):
			return False
		a = self
		for v in other:
			a = a.nxt
			if a is None or a.value != v:
				return False
		return a.nxt is None
	# delete the item at an index, and return its value
	# if no index is given, use the end of the list.
	def pop(self, index=None):
		if index is None:
			if self.nxt is None: raise IndexError('pop from empty list')
			a = self
			# find second-last item
			while a.nxt.nxt is not None:
				a = a.nxt
		elif isinstance(index, int):
			a = self if index == 0 else self.item_at_index(index-1)
			if a.nxt is None: raise IndexError('pop index out of range')
		else:
			raise TypeError('integer argument expected, got {0}'.format(type(index).__name__))
		x = a.nxt.value
		a.nxt = a.nxt.nxt
		return x
	# delete the first instance of the value x in the list
	def remove(self, x):
		a = self
		while a.nxt is not None:
			if a.nxt.value == x:
				a.nxt = a.nxt.nxt
				return
			a = a.nxt
		raise ValueError('LinkedList.remove(x): x not in list')
	# create a new list with the same values in reverse order, implements reversed(self)
	def __reversed__(self):
		b = LinkedList()
		a = self
		while a.nxt is not None:
			b.prepend(a.nxt.value)
			a = a.nxt
		return b
	# reverse the order of the list in place
	def reverse(self):
		self.nxt = self.__reversed__().nxt
	# sort the list in-place
	def sort(self):
		# rather than writing a sorting algorithm ourself here, let's just use Python's
		# built-in sorted() function, as a demonstration. sorted() doesn't notice that we
		# gave it a LinkedList instead of a built-in list type; Python is dynamically typed,
		# so "if it waddles like a duck and quacks like a duck, then it's a duck."
		self.nxt = LinkedList( sorted(self) ).nxt
	# create a shallow copy of the list
	def copy(self):
		a = self
		b_head = b = LinkedList()
		while a.nxt is not None:
			a = a.nxt
			b.nxt = ListItem(a.value)
			b = b.nxt
		return b_head
	# get a slice of the list from start to stop, in steps
	def get_slice(self, start=None, stop=None, step=None):
		if step is None:
			step = 1
		elif step == 0:
			raise ValueError('slice step cannot be zero')
		elif step < 0:
			n = len(self)
			if start is not None: start = n-1-start
			if stop is not None: stop = n-1-stop
			return self.__reversed__().get_slice(start, stop, -step)
		#
		n = len(self)
		if start is None:
			start = 0
		elif start < 0:
			start = max(0, start+n)
		if stop is None:
			stop = n
		elif stop < 0:
			stop = min(n, stop+n)
		try:
			a = self.item_at_index(start)
		except IndexError:
			return LinkedList()
		i = start
		b_head = b = LinkedList()
		while (stop is None or i < stop) and a is not None:
			b.nxt = ListItem(a.value)
			b = b.nxt
			j = 0
			while j < step and a is not None:
				a = a.nxt
				j += 1
			i += step
		return b_head
	# implements str(self)
	def __str__(self):
		output = 'LinkedList(['
		a = self
		while a.nxt is not None:
			a = a.nxt
			output += repr(a.value)
			if a.nxt is not None:
				output += ', '
		output += '])'
		return output
	# implements repr(self)
	def __repr__(self):
		return self.__str__()
	# create a new Iterator object, implements iter(self), used for (for v in self)
	def __iter__(self):
		return LinkedListIterator(self)
#

# implements (for x in self)
class LinkedListIterator:
	def __init__(self, L):
		self.a = L
	def __iter__(self):
		return self
	def __next__(self):
		if self.a.nxt is None:
			raise StopIteration()
		self.a = self.a.nxt
		return self.a.value
#
