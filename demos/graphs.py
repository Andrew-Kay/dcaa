from graphs import *

G = RelationGraph()

G.add_node('a')
G.add_node('b')
G.add_node('c')
G.add_node('d')

G.add_edge('a', 'b')
G.add_edge('a', 'c')
G.add_edge('b', 'c')
G.add_edge('c', 'd')

assert G.has_edge('a', 'b')
assert G.has_edge('d', 'c')
assert not G.has_edge('b', 'd')

assert 'b' in G.neighbours_of('a')
assert 'd' not in G.neighbours_of('a')
assert 'c' in G.neighbours_of('d')

assert G.is_connected()
assert not G.is_tree()


H = NeighbourGraph({ 'a': { 'b' }, 'b': { 'c' }, 'c': { 'd' }, 'd': { 'e' } })

assert H.nodes() == { 'a', 'b', 'c', 'd', 'e' }
assert H.edges() == {
	('a','b'), ('b', 'a'),
	('b','c'), ('c', 'b'),
	('c','d'), ('d', 'c'),
	('d','e'), ('e', 'd'),
}
assert H.is_tree()


J = AdjacencyMatrixGraph([
	[0, 3, 5, 2],
	[3, 0, 1, 9],
	[5, 1, 0, 0],
	[2, 9, 0, 0]
])

assert J.has_edge(0,1) and J.has_edge(1,2)
assert not J.has_edge(2,3)
assert J.shortest_path(2,3) == [2, 1, 0, 3]


def str_tree(x,y):
	return x == y[:-1] != y or x[:-1] == y != x
#
S = PredicateGraph({'','a','b','aa','ab','ba','bb'}, str_tree)

assert S.shortest_path('ab','ba') == ['ab','a','','b','ba']
assert S.is_tree()


def Paley_graph(p):
	squares = { i**2 % p for i in range(1,p) }
	return PredicateGraph(range(0,p), lambda x,y: (x-y)%p in squares)
#

P17 = Paley_graph(17)
# shortest_path returns a list of nodes, so len(...) is 1 more than the number of edges
assert all( len( P17.shortest_path(a,b) ) <= 3 for a in P17.nodes() for b in P17.nodes() )
