from groups import *
from sets import Function

S4 = symmetric_group(4)
A4 = alternating_group(4)

a = Function({ (0,1), (1,2), (2,3), (3,0) })
b = Function({ (0,1), (1,0), (2,3), (3,2) })

assert a * b == b * a**-1

C4 = Group.generated_by(a)
D4 = Group.generated_by(a, b)
K4 = Group.generated_by(b, a*a)

assert K4 <= A4 <= S4
assert C4 <= D4 <= S4

assert K4.is_abelian()
assert not D4.is_abelian()

D3 = dihedral_group(3)
S3 = symmetric_group(3)
assert D3 == S3
assert D4 != S4
assert not S3.is_abelian()

C2 = cyclic_group(2)
assert (C2*C2).relabelled() == K4
assert (C2*C4).is_abelian()

G = Group({1,-1})
assert G.identity == 1

H = Group(range(0,4), op=lambda x,y: (x+y)%4)
assert H.identity == 0
assert H.is_abelian()
assert H.relabelled() == C4

J = Group({1,2,3,4}, op=lambda x,y: (x*y)%5)
K = Group({1,3,5,7}, op=lambda x,y: (x*y)%8)

assert J.subgroup_generated_by(3) == J
assert J.generating_set() < {2,3}
assert K.subgroup_generated_by(3) != K
assert len(K.generating_set()) == 2
