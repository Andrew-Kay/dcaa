from sets import *

people = { 'Alice', 'Bob', 'Charles', 'Diane', 'Eve', 'Fred', 'George' }
managers = { 'Alice', 'Eve' }
places = { 'Birmingham', 'Coventry', 'London' }
numbers = range(1000)

ids = {
	(111, 'Alice'),
	(222, 'Bob'),
	(333, 'Charles'),
	(444, 'Diane'),
	(555, 'Eve')
}
is_friends_with = {
	('Alice', 'Bob'), ('Bob', 'Alice'),
	('Alice', 'Diane'), ('Diane', 'Alice'),
	('Alice', 'Eve'), ('Eve', 'Alice'),
	('Bob', 'Eve'), ('Eve', 'Bob'),
	('Charles', 'Diane'), ('Diane', 'Charles'),
	('Eve', 'George'), ('George', 'Eve')
}
lives_in = {
	('Alice', 'Birmingham'),
	('Bob', 'Coventry'),
	('Charles', 'Coventry'),
	('Diane', 'Birmingham'),
	('Eve', 'Birmingham'),
	('Fred', 'London'),
	('George', 'London')
}
