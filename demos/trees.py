from trees import *

a = BinaryTree(value='a')
b = a.add_child('b')
c = a.add_child('c')
d = b.add_child('d')
e = c.add_child('e')
f = c.add_child('f')

#>>> print(a)
#'a' +-> 'b' +-> 'd'
#    |
#    +-> 'c' +-> 'e'
#            |
#            +-> 'f'

assert d.parent() == b
assert a.children() == [b, c]
assert a.descendants() == [a, b, d, c, e, f]
assert d in b.children()
assert e not in a.children()
assert a in c.ancestors()
assert e not in c.ancestors()


B = ListTree([ [ [1,2,3], [4,5] ], [ 6, [7,8] ], [ 9 ], [ [10,11], 12 ] ])
assert B.values() == list( range(1,13) )

#>>> print(B)
#ListTree +-> ListTree +-> ListTree +-> 1
#         |            |            |
#         |            |            +-> 2
#         |            |            |
#         |            |            +-> 3
#         |            |
#         |            +-> ListTree +-> 4
#         |                         |
#         |                         +-> 5
#         |
#         +-> ListTree +-> 6
#         |            |
#         |            +-> ListTree +-> 7
#         |                         |
#         |                         +-> 8
#         |
#         +-> ListTree +-> 9
#         |
#         +-> ListTree +-> ListTree +-> 10
#                      |            |
#                      |            +-> 11
#                      |
#                      +-> 12


C = BinaryTree()
def extend(tree):
	global counter
	for leaf in tree.leaf_nodes():
		leaf.add_child()
		leaf.add_child()
#

extend(C)
extend(C)
extend(C)

C_nodes = C.descendants()
assert len(C_nodes) == 15

for i in range(0,15):
	C_nodes[i].value = i
#

#>>> print(C)
#0 +-> 1 +-> 2 +-> 3
#  |     |     |
#  |     |     +-> 4
#  |     |
#  |     +-> 5 +-> 6
#  |           |
#  |           +-> 7
#  |
#  +-> 8 +-> 9 +-> 10
#        |     |
#        |     +-> 11
#        |
#        +-> 12 +-> 13
#               |
#               +-> 14

G = C.to_graph()
assert G.is_connected()
assert G.is_tree()

def path(x,y):
	node_x, node_y = C_nodes[x], C_nodes[y]
	path = G.shortest_path(node_x, node_y)
	return [ C_nodes.index(a) for a in path ]
#

assert path(2,13) == [2, 1, 0, 8, 12, 13]
