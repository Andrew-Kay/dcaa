>>> from linkedlist import *
>>> A = LinkedList()
>>> A.append('a')
>>> A.append('b')
>>> A.append('c')
>>> A.append('d')
>>> A
LinkedList(['a', 'b', 'c', 'd'])
>>> A[2]
'c'
>>> A[1] = 'f'
>>> A
LinkedList(['a', 'f', 'c', 'd'])
>>> A.prepend('g')
>>> A
LinkedList(['g', 'a', 'f', 'c', 'd'])
>>> A.index('c')
3
>>> 'c' in A
True
>>> 'h' in A
False
>>> A[1] += 'pple'
>>> A
LinkedList(['g', 'apple', 'f', 'c', 'd'])
>>> B = LinkedList([ 'e', 'g', 'g'  ])
>>> B
LinkedList(['e', 'g', 'g'])
>>> A.extend(B)
>>> A
LinkedList(['g', 'apple', 'f', 'c', 'd', 'e', 'g', 'g'])
>>> A.count('g')
3
>>> A.count('c')
1
>>> A.count('x')
0
>>> del A[4]
>>> A
LinkedList(['g', 'apple', 'f', 'c', 'e', 'g', 'g'])
>>> A.remove('apple')
>>> A
LinkedList(['g', 'f', 'c', 'e', 'g', 'g'])
>>> A.reverse()
>>> A
LinkedList(['g', 'g', 'e', 'c', 'f', 'g'])
>>> from random import shuffle
>>> shuffle(A)
>>> A
LinkedList(['f', 'e', 'c', 'g', 'g', 'g'])
>>> A.sort()
>>> A
LinkedList(['c', 'e', 'f', 'g', 'g', 'g'])
