nugget_sizes = [20, 9, 6]

# the `tracer` module allows us to see what the algorithms do
# after calling one of these functions, use print_call_tree() to see the call tree
from tracer import *

# recursive solution
@trace_calls
def nuggets(n, sizes=nugget_sizes):
	# solve the base case
	if n == 0: return []
	for c in sizes:
		# only try sizes which don't make the total too big
		if c <= n:
			# n-c is "closer" to the base case than n is
			L = nuggets(n-c, sizes)
			# if it leads to a solution, then return it
			if L is not None:
				return [c] + L
			# otherwise backtrack and try the next size
	# if we run out of sizes to try, backtrack further
	# if this isn't a recursive call, then we're backtracking from an empty list, so there is no solution
	return None
#

# non-recursive solution
def nuggets_without_recursion(n, sizes=nugget_sizes):
	if n == 0: return []
	# this algorithm needs sizes to be in descending order
	sizes = sorted(sizes, reverse=True)
	# equivalent to L = [], but now we print_call_tree() shows everything that happens to L
	L = tracer([])
	# keep going until the total is correct
	while sum(L) < n:
		# find the sizes which don't make the total too big
		possibles = [ c for c in sizes if sum(L)+c <= n ]
		if len(possibles) > 0:
			# extend the list with the largest possible size
			L.append(possibles[0])
		else:
			# dead end, backtrack
			while len(L) > 0:
				c = L.pop()
				if c != sizes[-1]:
					# try the next-smaller size
					i = sizes.index(c)
					L.append(sizes[i+1])
					break
				# otherwise, if there is no next size to try, backtrack further
			# if we backtrack from an empty list, there is no solution
			if len(L) == 0: return None
	# the loop ends when sum(L) >= n. since we never allow sum(L) > n, this L is a solution.
	return L
#

def print_nugget_solutions(up_to = 50, sizes=nugget_sizes):
	print('\n'.join(
		'{0}: {1}'.format(n, nuggets_without_recursion(n, sizes))
		for n in range(up_to+1)
	))
#

# neither of these algorithms takes advantage of symmetry:
# e.g. if we tried [20, ...] and found that doesn't work, next we try [9, ...]
# since we know 20 can't exist in the solution, there is no need to try [9, 20, ...]

# Exercise: try modifying each algorithm to take advantage of symmetry.
# How much more efficient does it make the algorithm? Is it asymptotically more efficient?
