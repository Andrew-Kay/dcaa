from correctness import *
from logic import in_order

@input_type(int, int)
@output_type(int)
@precondition( lambda x,y: x <= y )
@postcondition( lambda output,x,y: x + output == y )
def difference(x,y):
	return y - x
#

@input_type(name=str)
@output_type(str)
def say_hello(name='Andrew'):
	return 'Hello, {0}!'.format(name)
#

@input_type(int, int)
@output_type(int)
@precondition( lambda x,y: y >= 0 )
@postcondition( lambda output,x,y: output == x*y )
def multiply_by_adding(x,y):
	r = 0
	correct_result = x*y
	# check correctness
	invariant = Invariant( lambda: r + x*y == correct_result )
	variant = LoopVariant( lambda: y )
	while y > 0:
		r += x
		y -= 1
		# check correctness
		invariant.check()
		variant.check()
	return r
#

@input_type(int)
@output_type(int)
@precondition( lambda x: x >= 0 )
@postcondition( lambda output,x: output**2 <= x < (output+1)**2 )
def int_sqrt(x):
	r = 0
	t = 0
	# check correctness
	invariant = Invariant( lambda: t == r**2 )
	variant = LoopVariant( lambda: x - t )
	while t + 2*r + 1 <= x:
		t += 2*r + 1
		r += 1
		# check correctness
		invariant.check()
		variant.check()
	return r
#

@input_type(int)
@output_type(int)
@precondition( lambda n: n >= 0 )
@recursion_variant( lambda n: n )
def fibonacci(n): # this is an inefficient implementation, since it will evaluate each fibonacci(k) many times
	if(n <= 1): # base case
		return 1
	else:
		return fibonacci(n-1) + fibonacci(n-2)
#

@input_type(list, int)
@output_type(int, or_none=True)
@postcondition( lambda output,L,x: (output is None and x not in L) or (output is not None and L[output] == x) )
def linear_search(L,x):
	i = 0
	n = len(L)
	# check correctness
	invariant = Invariant( lambda: x not in L[:i] )
	variant = LoopVariant( lambda: n - i )
	while i < n:
		if L[i] == x:
			return i
		i += 1
		# check correctness
		invariant.check()
		variant.check()
	return None
#

@input_type(list)
@precondition( lambda L: len(L) >= 1 )
@postcondition( lambda output,L: output in L )
@postcondition( lambda output,L: all( output <= x for x in L ) )
def linear_search_min(L):
	m = L[0]
	i = 1
	n = len(L)
	# check correctness
	invariant = Invariant( lambda: all( m <= x for x in L[:i] ) )
	variant = LoopVariant( lambda: n - i )
	while i < n:
		if L[i] < m:
			m = L[i]
		i += 1
		# check correctness
		invariant.check()
		variant.check()
	return m
#

@input_type(list)
@precondition( lambda L: len(L) >= 1 )
@postcondition( lambda output,L: output in L )
@postcondition( lambda output,L: all( output >= x for x in L ) )
def linear_search_max(L):
	m = L[0]
	i = 1
	n = len(L)
	# check correctness
	invariant = Invariant( lambda: all( m >= x for x in L[:i] ) )
	variant = LoopVariant( lambda: n - i )
	while i < n:
		if L[i] > m:
			m = L[i]
		i += 1
		# check correctness
		invariant.check()
		variant.check()
	return m
#

@input_type(list, int)
@output_type(int, or_none=True)
@precondition( lambda L,x: in_order(L) )
@postcondition( lambda output,L,x: (output is None and x not in L) or (output is not None and L[output] == x) )
def binary_search(L,x):
	a,b = 0, len(L)-1
	# check correctness
	invariant = Invariant( lambda: x not in L or L[a] <= x <= L[b] )
	variant = LoopVariant( lambda: b + 1 - a )
	while a <= b:
		m = (a+b) // 2
		if x == L[m]:
			return m
		elif x < L[m]:
			b = m-1
		else:
			a = m+1
		# check correctness
		invariant.check()
		variant.check()
	return None
#

@input_type(list)
@output_type(list)
@postcondition( lambda output,L: in_order(output) )
def selection_sort(L):
	n = len(L)
	i = 0
	# check correctness of outer loop
	order_invariant = Invariant( lambda: in_order(L[:i]) )
	paritition_invariant = Invariant( lambda: all( r <= s for r in L[:i] for s in L[i:] ) )
	variant = LoopVariant( lambda: n - i )
	while i < n-1:
		m = i
		j = i+1
		# check correctness of inner loop
		search_invariant = Invariant( lambda: all( L[m] <= v for v in L[i:j] ) )
		search_variant = LoopVariant( lambda: n - j )
		while j < n:
			if L[j] < L[m]:
				m = j
			j += 1
			# check correctness of inner loop
			search_invariant.check()
			search_variant.check()
		L[i], L[m] = L[m], L[i]
		i += 1
		# check correctness of outer loop
		order_invariant.check()
		paritition_invariant.check()
		variant.check()
	return L
#

@input_type(list)
@output_type(list)
@postcondition( lambda output,L: in_order(output) )
def bubble_sort(L):
	n = len(L)
	i = 0
	# check correctness of outer loop
	order_invariant = Invariant( lambda: in_order(L[n-i:]) )
	partition_invariant = Invariant( lambda: all( r <= s for r in L[:n-i] for s in L[n-i:]) )
	variant = LoopVariant( lambda: n - i )
	swaps = True
	while i < n-1 and swaps:
                swaps = False
		j = 0
		# check correctness of inner loop
		bubble_invariant = Invariant( lambda: all( v <= L[j] for v in L[:j] ) )
		bubble_variant = LoopVariant( lambda: n - i - j )
		no_swaps_invariant = Invariant( lambda: swaps or in_order(L[:j]) )
		while j < n-1-i:
			if L[j] > L[j+1]:
				L[j], L[j+1] = L[j+1], L[j]
				swaps = True
			j += 1
			# check correctness of inner loop
			bubble_invariant.check()
			bubble_variant.check()
		i += 1
		# check correctness of outer loop
		order_invariant.check()
		partition_invariant.check()
		variant.check()
	return L
#

@input_type(list)
@output_type(list)
@postcondition( lambda output,L: in_order(output) )
def insertion_sort(L):
	n = len(L)
	i = 1
	# check correctness of outer loop
	order_invariant = Invariant( lambda: in_order(L[:i]) )
	variant = LoopVariant( lambda: n + 1 - i ) # add one because L could be empty
	while i < n:
		j = i
		# check correctness of inner loop
		insertion_invariant = Invariant( lambda: all( L[j] < v for v in L[j+1:i+1] ) )
		insertion_variant = LoopVariant( lambda: j )
		while j > 0 and L[j-1] > L[j]:
			L[j-1], L[j] = L[j], L[j-1]
			j -= 1
			# check correctness of inner loop
			insertion_invariant.check()
			insertion_variant.check()
		i += 1
		# check correctness of outer loop
		order_invariant.check()
		variant.check()
	return L
#

@input_type(list, list)
@output_type(list)
@precondition( lambda A,B: in_order(A) and in_order(B) )
@postcondition( lambda output,A,B: in_order(output) )
def linear_merge(A,B):
	a,b = len(A), len(B)
	n = a + b
	L = [0]*n
	i,j = 0,0
	# check correctness
	invariant = Invariant( lambda: in_order(L[:i+j]) )
	variant = LoopVariant( lambda: n - i - j )
	while i + j < n:
		if i < a and (j == b or A[i] <= B[j]):
			L[i+j] = A[i]
			i += 1
		else:
			L[i+j] = B[j]
			j += 1
		# check correctness
		invariant.check()
		variant.check()
	return L
#

@input_type(list)
@output_type(list)
@postcondition( lambda output,L: in_order(output) )
@recursion_variant( lambda L: len(L) )
def merge_sort(L):
	n = len(L)
	if n <= 1: # base case
		return list(L)
	m = n // 2
	A = L[:m]
	B = L[m:]
	A = merge_sort(A)
	B = merge_sort(B)
	return linear_merge(A,B)
#

@input_type(list)
@output_type(list)
@postcondition( lambda output,L: in_order(output) )
@recursion_variant( lambda L: len(L) )
def quicksort(L):
	n = len(L)
	if n <= 1: # base case
		return list(L)
	p = L[0]
	A,B = [],[]
	i = 1
	# check correctness
	partition_A_invariant = Invariant( lambda: all( v <= p for v in A ) )
	partition_B_invariant = Invariant( lambda: all( v > p for v in B ) )
	length_invariant = Invariant( lambda: len(A)+len(B)+1 == i )
	loop_variant = LoopVariant( lambda: n-i )
	while i < n:
		v = L[i]
		if v <= p:
			A.append(v)
		else:
			B.append(v)
		i += 1
		# check correctness
		partition_A_invariant.check()
		partition_B_invariant.check()
		length_invariant.check()
		loop_variant.check()
	A = quicksort(A)
	B = quicksort(B)
	return A + [p] + B
#
