import sys
from copy import deepcopy 
from functools import wraps
from trees import ListTree

class CallInfo: # record info about a function call
	def __init__(self, obj, f, vargs, kwargs, returned=None, raised=None):
		self.obj = obj
		self.f = f
		self.vargs = deepcopy(vargs)
		self.kwargs = deepcopy(kwargs)
		self.returned = deepcopy(returned)
		self.raised = raised
	def argstring(self):
		return ', '.join([repr(v) for v in self.vargs] + ['{0}={1}'.format(k,repr(v)) for (k,v) in self.kwargs.items()])
	def __repr__(self):
		return '{0}{1}({2})'.format(CallInfo.object_id(self.obj), self.f.__name__, self.argstring())
	@staticmethod
	def object_id(obj):
		return '' if obj is None else '{0}@0x{1:08X}.'.format(obj.__class__.__name__, id(obj))
#
class CallTree(ListTree):
	def for_function(self, f):
		filter_predicate = lambda n: (n.value.f is f) if isinstance(n.value, CallInfo) else (n.parent().value.f is f)
		return self.filter(filter_predicate)
	def for_object(self, obj):
		filter_predicate = lambda n: (n.value.obj is obj) if isinstance(n.value, CallInfo) else (n.parent().value.obj is obj)
		tr = self.filter(filter_predicate)
		for n in tr.descendants():
			if isinstance(n.value, CallInfo):
				n.value = CallInfo(None, n.value.f, n.value.vargs, n.value.kwargs, n.value.returned, n.value.raised)
		return tr
#
class Tracer: # object for logging function calls in a call tree
	def __init__(self):
		self.call_tree = CallTree()
		self.current_node = self.call_tree
		self.is_suspended = 0
	def clear_call_tree(self):
		self.call_tree.child_list = []
		self.current_node = self.call_tree
	def print_call_tree(self):
		print(self.call_tree)
	def push(self, ci):
		if not self.is_suspended:
			self.current_node = self.current_node.add_child(ci)
	def pop(self, returned=None, raised=None):
		if not self.is_suspended:
			if returned is not None:
				returned = deepcopy(returned)
				self.current_node.value.returned = returned
				self.current_node.add_child(returned)
			elif raised is not None:
				self.current_node.value.raised = raised
				self.current_node.add_child('raised {0}'.format(repr(raised)))
			self.current_node = self.current_node.parent()
	def suspend(self):
		self.is_suspended += 1
	def unsuspend(self):
		self.is_suspended -= 1
	def log_call(self, obj, f, vargs, kwargs):
		if not self.is_suspended:
			ci = CallInfo(obj, f, vargs, kwargs)
			self.push(ci)
		try:
			if obj is None:
				output = f(*vargs, **kwargs)
			else:
				output = f(obj, *vargs, **kwargs)
			self.pop(returned=output)
			return output
		except:
			e = sys.exc_info()[0]
			self.pop(raised=e)
			raise
#

__global_tracer__ = Tracer()
def print_call_tree(t=None):
	if t is None:
		__global_tracer__.print_call_tree()
	elif hasattr(t, 'print_call_tree'):
		t.print_call_tree()
	else:
		raise ValueError()
def clear_call_tree(t=None):
	if t is None:
		__global_tracer__.clear_call_tree()
	elif hasattr(t, 'clear_call_tree'):
		t.clear_call_tree()
	else:
		raise ValueError()
#

def trace_calls(f): # decorator to trace function calls
	return use_tracer(__global_tracer__)(f)
def use_tracer(tracer_obj): # decorator to trace function calls to a given tracer object
	tracer_obj = __get_tracer_of__(tracer_obj)
	def decorator(f):
		@wraps(f)
		def func_wrapper(*vargs, **kwargs):
			return tracer_obj.log_call(None, f, vargs, kwargs)
		func_wrapper.__tracer__ = tracer_obj
		func_wrapper.print_call_tree = lambda: print(tracer_obj.call_tree.for_function(f))
		func_wrapper.clear_call_tree = tracer_obj.clear_call_tree
		return func_wrapper
	return decorator
#

# returns a dynamically-created subclass (or proxy object) which logs method calls
def tracer(cls, methods=None, tracer_obj=None):
	if not isinstance(cls, type) and hasattr(cls, '__class__'):
		return tracer(cls.__class__)(cls) # if input is an object, return an object
	proxy_cls = type(cls.__name__, (cls,TracerProxy), dict())
	d = set(dir(cls)) - { # don't wrap these attributes
		'__new__', '__init__', '__setattr__', '__getattribute__', '__delattr__',
		'__class__', '__dir__', '__dict__', '__sizeof__', '__repr__', '__iter__',
		'__reduce__', '__reduce_ex__', '__getstate__', '__setstate__', '__deepcopy__',
	}
	if methods is not None:
		d &= set(methods)
	for a in d:
		v = getattr(cls, a)
		t = type(cls.__dict__[a] if a in cls.__dict__ else v).__name__
		if t in ['function', 'method', 'method_descriptor', 'wrapper_descriptor']:
			v = trace_method_calls(v)
		setattr(proxy_cls, a, v)
	def proxy_init(obj, *vargs, **kwargs):
		obj.__tracer__ = __global_tracer__ if tracer_obj is None else tracer_obj
		obj.__trace_suspended__ = 0
		if hasattr(cls, '__init__'):
			obj.__tracer__.log_call(obj, cls.__init__, vargs, kwargs)
	proxy_cls.__init__ = proxy_init
	proxy_cls.__traced_class__ = cls
	proxy_cls.__tracer__ = tracer_obj
	return proxy_cls
#
class TracerProxy:
	def print_call_tree(self):
		print(self.__tracer__.call_tree.for_object(self))
	def clear_call_tree(self):
		self.__tracer__.clear_call_tree()
	def __reduce_ex__(self, n):
		# super().__reduce_ex__ calls __iter__, which is traced by log_call; log_call calls deepcopy, which calls __reduce_ex__.
		# therefore, to avoid infinite recursion we must suspend tracing while we evaluate super().__reduce_ex__
		self.__tracer__.suspend()
		o = super().__reduce_ex__(n)
		self.__tracer__.unsuspend()
		return o
#
def trace_method_calls(f): # decorator to trace method calls
	@wraps(f)
	def func_wrapper(obj, *vargs, **kwargs):
		return obj.__tracer__.log_call(obj, f, vargs, kwargs)
	return func_wrapper
#

def __get_tracer_of__(obj):
	if isinstance(obj, Tracer):
		return obj
	elif hasattr(obj, '__tracer__'):
		return obj.__tracer__
	else:
		raise TypeError('obj must be a Tracer object or another traced function or object')
#

