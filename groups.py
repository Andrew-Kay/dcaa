from sets import *

class Group(frozenset):
	@staticmethod
	def generated_by(*v, underlying_set=None, op=None, act=None):
		if op is None: op = lambda f,g: f*g
		G = set()
		newG = set(v)
		while len(newG) > 0:
			G |= newG
			newG = { op(f,g) for f in newG for g in v } - G
		return Group(G, underlying_set, op, act)
	#
	
	def __new__(cls, elements, underlying_set=None, op=None, act=None):
		return super(Group, cls).__new__(cls, elements)
	def __init__(self, elements, underlying_set=None, op=None, act=None):
		self.op = (lambda f,g: f*g) if op is None else op
		if all( isinstance(f,Function) for f in elements ):
			self.underlying_set = take_from(self).source
			if not all( f.source == self.underlying_set and f.target == self.underlying_set for f in self ):
				raise ValueError('Group elements must all have the same source and target.')
			self.act = (lambda f,x: f(x)) if act is None else act
		else:
			self.underlying_set = self if underlying_set is None else underlying_set
			self.act = self.op if act is None else act
		#
		if not all( self.op(f,g) in self for f in self for g in self ):
			raise ValueError('Not closed under group operation.')
		I = [ e for e in self if all(self.op(e,f) == f and self.op(f,e) == f for f in self) ]
		if len(I) != 1:
			raise ValueError('Missing identity element.')
		self.identity = I[0]
		#if not all( self.op(f,self.op(g,h)) == self.op(self.op(f,g),h) for f in self for g in self for h in self ):
		#	raise ValueError('Group operation not associative.')
		if not all( any( self.op(f,g) == self.identity for g in self ) for f in self ):
			raise ValueError('Missing inverse element.')
	#
	
	def subgroup_generated_by(self,*v):
		if len(v) == 0: v = [ self.identity ]
		return Group.generated_by(*v, underlying_set=self.underlying_set, op=self.op, act=self.act)
	def cayley_table(self):
		G = list(self)
		if all( isinstance(f, Function) for f in G ):
			G.sort(key=lambda f: sorted(f))
		else:
			G.sort()
		w = max( len(str(f)) for f in G )
		return '\n'.join( ' '.join( '{0: ^{1}}'.format(str(self.op(f,g)), w) for g in G ) for f in G )
	def __mul__(self,other):
		elements = product(self, other)
		underlying_set = product(self.underlying_set, other.underlying_set)
		op = lambda f,g: ( self.op(f[0],g[0]), other.op(f[1],g[1]) )
		act = lambda f,x: ( self.act(f[0],x[0]), other.act(f[1],x[1]) )
		return Group(elements, underlying_set, op, act)
	def is_abelian(self):
		return all( self.op(f,g) == self.op(g,f) for f in self for g in self )
	def generating_set(self):
		n = len(self)
		generators = { take_from(self) }
		while len(self.subgroup_generated_by(*generators)) < n:
			g = take_from(self - generators)
			generators.add(g)
		for g in generators:
			fewer_generators = generators - {g}
			if len(self.subgroup_generated_by(*fewer_generators)) == n:
				generators = fewer_generators
		return generators
	def regular_representation(self):
		return Group({ Function({ (h, self.op(g,h)) for h in self }, self, self) for g in self })
	def relabelled(self):
		A = sorted(self.underlying_set)
		n = len(A)
		A_to_i = { A[i]: i for i in range(0, n) }
		return Group({ Function({ (A_to_i[x], A_to_i[self.act(f,x)]) for x in self.underlying_set }) for f in self })
	def cayley_graph(self, generators=None):
		if generators is None:
			generators = self.generating_set()
		elif any( f not in self for f in generators ):
			raise ValueError('Generators not all in the group.')
		return Relation({ (h, self.op(g,h)) for h in self for g in generators })
#

def signature(f):
	return (-1)**sum( 1 for i in f.source for j in f.source if i<j and f(i)>f(j) )
#

def symmetric_group(n):
	source = frozenset( range(0,n) )
	return Group( f for f in all_functions(source,source) if f.is_bijective() )
#
def alternating_group(n):
	return Group( f for f in symmetric_group(n) if signature(f) == 1 )
#
def cyclic_group(n):
	f = Function({ (i,i+1) for i in range(0,n-1) }|{ (n-1,0) })
	return Group.generated_by(f)
#
def dihedral_group(n):
	f = Function({ (i,i+1) for i in range(0,n-1) }|{ (n-1,0) })
	g = Function({ (i,n-1-i) for i in range(0, n) })
	return Group.generated_by(f,g)
#
