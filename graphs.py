from sets import take_from, Relation

# only simple graphs (undirected, no loops) are represented

class Graph:
	def __str__(self):
		matrix = self.adjacency_matrix()
		w = len( str(max( max(row) for row in matrix)) )
		return '\n'.join( ' '.join( '{0:{1}d}'.format(x,w) for x in row ) for row in matrix )
	def isolated_nodes(self):
		return { v for v in self.nodes() if len(self.neighbours_of(v)) == 0 }
	def is_connected(self):
		nodes = self.nodes()
		if len(nodes) == 0: return True
		v = take_from(nodes)
		return self.connected_component_of(v) == nodes
	def is_tree(self):
		# a graph is a tree if and only if it has n-1 edges and no isolated nodes
		# divide by 2 since (v,w) and (w,v) are both in self.edges()
		return len(self.edges())/2 == len(self.nodes())-1 and len(self.isolated_nodes()) == 0
	#
	
	def connected_component_of(self, v):
		if v not in self.nodes(): raise ValueError('node not present')
		component = set()
		new_nodes = { v }
		while len(new_nodes) > 0:
			component |= new_nodes
			new_neighbours = set()
			for v in new_nodes:
				new_neighbours |= self.neighbours_of(v)
			new_nodes = new_neighbours - component
		return component
	#
	
	def minimum_spanning_tree(self):
		# Kruskal's algorithm
		nodes = self.nodes()
		n = len(nodes)
		# make a copy of the graph with no edges
		if isinstance(self, MutableGraph):
			result = self.__class__()
		else:
			result = NeighbourGraph() # if self is immutable, use a mutable type
		for v in nodes:
			result.add_node(v)
		# sort the edges by weight
		edges = sorted(self.weighted_edges(), key=lambda t: t[2])
		# an acyclic graph is connected if and only if it has n-1 edges
		no_of_edges = 0
		for (v,w,weight) in edges:
			# adding the edge (v,w) creates a cycle if and only if w is in the connected component of v
			# a more efficient implementation would use a union-find data structure
			if w not in result.connected_component_of(v):
				result.add_edge(v, w, weight)
				no_of_edges += 1
				if no_of_edges == n - 1: # result is connected
					return result
		# if we run out of edges, then there is no spanning tree
		raise ValueError('graph is not connected')
	def shortest_path(self, start, end):
		# Dijkstra's algorithm
		confirmed_nodes = set()
		distance = dict()
		previous_in_path = dict()
		distance[start] = 0
		previous_in_path[start] = None
		while end not in confirmed_nodes:
			# choose an unconfirmed node of shortest distance
			unconfirmed_nodes = [ v for v in distance if v not in confirmed_nodes ]
			if len(unconfirmed_nodes) == 0:
				raise ValueError('no path found')
			v = min(unconfirmed_nodes, key=lambda v: distance[v])
			# shortest path to v is confirmed
			confirmed_nodes.add(v)
			# consider v's neighbours
			for w in self.neighbours_of(v):
				weight = self.edge_weight(v,w)
				new_distance = distance[v] + weight
				if w not in distance or distance[w] > new_distance: # is this a shorter path to w?
					distance[w] = new_distance
					previous_in_path[w] = v
		# reconstruct path from end to start
		path = []
		v = end
		while v is not None:
			path = [v] + path
			v = previous_in_path[v]
		return path
	#
	
	def __eq__(self,other):
		return self.nodes() == other.nodes() and self.weighted_edges() == other.weighted_edges()
#

class MutableGraph(Graph): pass
class ImmutableGraph(Graph):
	def add_node(self,v): raise NotImplementedError()
	def delete_node(self,v): raise NotImplementedError()
	def add_edge(self,v,w,weight=1): raise NotImplementedError()
	def delete_edge(self,v,w): raise NotImplementedError()
#

# below are various data structures for simple graphs

# represent a graph as a set of nodes, and a set of edges : nodes <-> nodes
class RelationGraph(MutableGraph):
	def __init__(self, R=None):
		R = Relation(R)
		if any( (v,v) in R for v in R.source ):
			raise ValueError('simple graph cannot contain self-edges')
		elif R.source != R.target:
			nodes = R.source | R.target
			R = Relation(R, nodes, nodes)
		self.relation = R.symmetric_closure()
	def nodes(self):
		return self.relation.source
	def edges(self):
		return self.relation
	def weighted_edges(self):
		return { (v,w,1) for (v,w) in self.relation }
	def has_edge(self, v, w):
		return (v,w) in self.relation
	def edge_weight(self, v, w):
		return 1 if (v,w) in self.relation else 0
	def neighbours_of(self, v):
		return self.relation.image_of({ v }) # relational image
	#
	def add_node(self, v):
		nodes = self.relation.source
		if v in nodes: raise ValueError('node already present')
		nodes |= {v}
		self.relation = Relation(self.relation, nodes, nodes)
		return v
	def delete_node(self, v):
		nodes = self.relation.source
		if v not in nodes: raise ValueError('node already present')
		nodes -= {v}
		edges = { (x,y) for (x,y) in self.relation if x != v and y != v }
		self.relation = Relation(edges, nodes, nodes)
	def add_edge(self, v, w, weight=1):
		if weight != 1: raise ValueError('{0} does not support weighted edges'.format(self.__class__))
		if v == w: raise ValueError('simple graph cannot contain self-edges')
		nodes = self.relation.source
		if v not in nodes or w not in nodes: ValueError('node not present')
		edges = self.relation
		if (v,w) in edges: raise ValueError('edge already present')
		self.relation = Relation(edges | { (v,w), (w,v) }, nodes, nodes)
	def delete_edge(self, v, w):
		nodes = self.relation.source
		if v not in nodes or w not in nodes: ValueError('node not present')
		edges = self.edges()
		if (v,w) not in edges: raise ValueError('edge not present')
		self.relation = Relation(edges - { (v,w), (w,v) }, nodes, nodes)
	#
	def isolated_nodes(self):
		return self.relation.source - self.relation.domain() - self.relation.range()
	def adjacency_matrix(self):
		node_list = sorted(self.relation.source)
		n = len(node_list)
		# use a dict to lookup the index of a node; more efficient than node_list.index(node)
		node_to_index = { node_list[i]: i for i in range(0,n) }
		# for each edge, set the corresponding matrix entry to 1
		matrix = [ [0]*n for i in range(0,n) ]
		for (v,w) in self.relation:
			matrix[ node_to_index[v] ][ node_to_index[w] ] = 1
		return matrix
	def __repr__(self):
		return '{0}({1}, {2})'.format( self.__class__.__name__, self.relation.source, self.relation )
#

# represent a graph as a dict, mapping nodes to dicts which map neighbours to edge weights
# in type theory, the data structure is like (node -> (node -> int)) instead of (node*node -> int)
class NeighbourGraph(MutableGraph):
	def __init__(self, neighbours=None):
		if neighbours is None: neighbours = dict()
		for v,N in list(neighbours.items()):
			if not isinstance(N, dict):
				neighbours[v] = N = { w: 1 for w in N }
			for w in N:
				if w not in neighbours:
					neighbours[w] = dict()
		for v,N in list(neighbours.items()):
			for w,weight in N.items():
				neighbours[w][v] = weight
		self.neighbours = neighbours
	def nodes(self):
		return set(self.neighbours.keys())
	def edges(self):
		return { (v,w) for (v,N) in self.neighbours.items() for w in N }
	def weighted_edges(self):
		return { (v,w,weight) for (v,N) in self.neighbours.items() for w,weight in N.items() }
	def has_edge(self, v, w):
		return v in self.neighbours and w in self.neighbours[v]
	def edge_weight(self, v, w):
		return self.neighbours[v][w] if self.has_edge(v,w) else 0
	def neighbours_of(self, v):
		return set( self.neighbours[ v ].keys() )
	#
	def add_node(self, v):
		if v in self.neighbours: raise ValueError('node already present')
		self.neighbours[v] = dict()
		return v
	def add_edge(self, v, w, weight=1):
		if v == w: raise ValueError('simple graph cannot contain self-edges')
		if weight <= 0: raise ValueError('weight must be positive')
		if v not in self.neighbours or w not in self.neighbours: ValueError('node not present')
		if w in self.neighbours[v]: raise ValueError('edge already present')
		self.neighbours[v][w] = weight
		self.neighbours[w][v] = weight
	def delete_edge(self, v, w):
		if v not in self.neighbours or w not in self.neighbours: ValueError('node not present')
		if w not in self.neighbours[v]: raise ValueError('edge not present')
		del self.neighbours[v][w]
		del self.neighbours[w][v]
	def delete_node(self, v):
		if v not in self.neighbours: raise ValueError('node not present')
		for w in self.neighbours_of(v):
			self.delete_edge(v,w)
		del self.neighbours[v]
	#
	def adjacency_matrix(self):
		node_list = sorted(self.neighbours.keys())
		n = len(node_list)
		# use a dict to lookup the index of a node; more efficient than node_list.index(node)
		node_to_index = { node_list[i]: i for i in range(0,n) }
		matrix = []
		for v in node_list:
			row = [0]*n
			for w in self.neighbours[v]:
				row[ node_to_index[w] ] = self.neighbours[v][w]
			matrix.append(row)
		return matrix
	def __repr__(self):
		return '{0}({1})'.format( self.__class__.__name__, self.neighbours )
#

# represent a weighted graph as an adjacency matrix
class AdjacencyMatrixGraph(MutableGraph):
	def __init__(self, matrix=None):
		self.matrix = [] if matrix is None else matrix
		n = len(self.matrix)
		if any( len(self.matrix[i]) != n for i in range(0,n) ):
			raise ValueError('adjacency matrix must be square')
		if any( self.matrix[i][i] for i in range(0,n) ):
			raise ValueError('simple graph cannot contain self-edges')
		for i in range(0,n):
			for j in range(0,n):
				if self.matrix[i][j] != 0:
					self.matrix[j][i] = self.matrix[i][j]
	def nodes(self):
		return range(0, len(self.matrix))
	def edges(self):
		nodes = self.nodes()
		return { (v,w) for v in nodes for w in nodes if self.has_edge(v,w) }
	def weighted_edges(self):
		nodes = self.nodes()
		return { (v,w,self.matrix[v][w]) for v in nodes for w in nodes if self.has_edge(v,w) }
	def has_edge(self, v, w):
		return self.matrix[v][w] != 0
	def edge_weight(self, v, w):
		return self.matrix[v][w]
	def neighbours_of(self, v):
		return { w for w in self.nodes() if self.has_edge(v,w) }
	#
	def add_node(self, v=None):
		n = len(self.matrix)
		if v >= 0 and v < n: raise ValueError('node not present')
		for row in self.matrix:
			row.append(0)
		self.matrix.append([0]*(n+1))
		return n
	def delete_node(self, v):
		n = len(self.matrix)
		if v < 0 or v >= n: raise ValueError('node not present')
		del self.matrix[v]
		for row in self.matrix:
			del row[v]
	def add_edge(self, v, w, weight=1):
		if v == w: raise ValueError('simple graph cannot contain self-edges')
		nodes = self.nodes()
		if v not in nodes or w not in nodes: ValueError('node not present')
		if weight <= 0: raise ValueError('weight must be positive')
		if self.has_edge(v,w): raise ValueError('edge already present')
		self.matrix[v][w] = weight
		self.matrix[w][v] = weight
	def delete_edge(self, v, w):
		nodes = self.nodes()
		if v not in nodes or w not in nodes: ValueError('node not present')
		if not self.has_edge(v,w): raise ValueError('edge not present')
		self.matrix[v][w] = 0
		self.matrix[w][v] = 0
	#
	def adjacency_matrix(self):
		return self.matrix
	def __repr__(self):
		return '{0}({1})'.format( self.__class__.__name__, self.matrix )
#

class PredicateGraph(ImmutableGraph):
	def __init__(self, nodes, predicate):
		self.nodeset = set(nodes)
		self.predicate = predicate
		if any( self.predicate(v,v) for v in self.nodeset ):
			raise ValueError('simple graph cannot contain self-edges')
		elif not all( self.predicate(w,v) for v in self.nodeset for w in self.nodeset if self.predicate(v,w) ):
			raise ValueError('simple graph must be symmetric')
	def nodes(self):
		return self.nodeset
	def edges(self):
		return { (x,y) for x in self.nodeset for y in self.nodeset if self.has_edge(x,y) }
	def weighted_edges(self):
		return { (x,y,1) for (x,y) in self.edges() }
	def has_edge(self,v,w):
		return self.predicate(v,w)
	def edge_weight(self,v,w):
		return 1 if self.has_edge(v,w) else 0
	def neighbours_of(self,v):
		return { w for w in self.nodeset if self.has_edge(v,w) }
	#
	def adjacency_matrix(self):
		nodes = list(self.nodes())
		return [ [ self.edge_weight(v,w) for w in nodes ] for v in nodes ]
#
